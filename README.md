Salt Light & Electric has offered expert lighting and electrical service for 35 years throughout Austin, Round Rock, Cedar Park, Pflugerville, Georgetown, and surrounding areas in Central Texas. Our experienced electricians can take care of any electrical repair or installation for your home or business, both indoors and outdoors. We also specialize in lighting services for homes and businesses. Contact us today to learn more!

Website: https://saltle.com
